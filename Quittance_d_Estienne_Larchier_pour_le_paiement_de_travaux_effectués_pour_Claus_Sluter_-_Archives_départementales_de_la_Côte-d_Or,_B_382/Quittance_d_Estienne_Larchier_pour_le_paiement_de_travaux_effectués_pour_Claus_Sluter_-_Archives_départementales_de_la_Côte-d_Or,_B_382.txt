Saichent tuit que je, Estienne Larchier, demorant a Dijon, confesse avoir eu et receu de Guillaume Chenily, receveur ou bailliage de Dijon
pour monseigneur le duc de Bourgoigne, la somme de sept frans demi qui deuz m’estoient pour pluseurs ouvraiges et parties de mon
mestier, par moy faites, baillees et delivrees, es hostelz de mondit seigneur a Dijon ou demeure Claux Scluter, ouvrier d’ymaiges de
mondit seigneur, et premierement pour avoir reffait de mes ais de chesne tout a nuef, le planchier de la saule devant
desdiz hostelz. Item reffaire et remettre a point les huys et fenestres, des ouvreurs dudit Claux qu’estoient despecies
Item faire quatre gros bans de grosses pieces de bois pour ouvrer dessus les ymaiges et pour une grrosse sebiere et
pourter pierres et ymaiges tout par marchié fait au dit receveur, en la presence dudit Claux aux diz VII frans demi
desquelx je me tien pour bien paié et content dudit receveur et l'en quitte et touz aultres a qui quittance en
peut et doit appartenir. En tesmoignage de ce, j’ay requis le seing manuel de Jehan le Bon de Dijon, clerc coadjoint du tabellion dudit
lieu pour mondit seigneur mis a ceste quittance le XXIIe jour du mois de decembre l’an mil CCC IIIIXX et seze. J. Le Bon
Et je, Claux Scluter, ouvrier d’ymaiges et varlet de chambre de monseigneur le duc de Bourgoigne, certiffie en vérité les parties
dessus dites et une chascune d’icelles avoir esté faites, baillees et delivrees esdiz hostelz es lieux dessus diz et tout par la
forme et maniere qui dessus est contenu par le dessus dit Estienne Larchier, par marchié fait a lui par le dit receveur
en ma presence a la somme de sept frans demi. Tesmoing mon seel mis a ces presentes l’an et jour dessus diz.


