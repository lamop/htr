Phelippe, filz de Roy de France, duc de Bourgoingne, conte de Flandres
d’Artois et de Bourgoingne, Palatin, sire de Salins, conte de Rethel
et seigneur de Malines. Savoir faisons a tous presens et advenir
que nous qui avons vraye congnoissance de la bonne loyaulté et
parfaicte amour que noz bien amez les habitans et commune de nostre
bonne ville de Dijon ont tousjours eu a nous, ont pour le present
et esperans que tousdiz auront, en consideracion aux bons services
et plaisirs que iceulx habitans et commune nous ont fais depuis que
nous venismes premiers a la seigneurie de nostre duchié de Bourgoigne
jusques a ores, voulans monstrer que nous avons congnoissance
des choses dessusdictes, et en donnant a eulx bonne voulenté de
tousjours mieulx faire pour plus honorer ladicte ville et les
habitans et commune d’icelle, a iceulx habitans et commune avons
octroyé, et par ces presentes octroyons que ès armes ou enseigne
de ladicte ville qui est ung escu de gueules tout plain, lesquelles
ilz ont accoustumé anciennement de porter, ilz puissent mectre
et porter perpetuellement en bataille et dehors, en tous les lieux
ou il leur plaira estre, mectre ou porter leursdictes armes
ou enseigne, ung chief de nos propres armes, au perpetuel
honneur et decorement d’icelles. Si donnons en mandement a
noz amez et feaulx mareschal et seneschal et a tous
nos autres justiciers et officiers, presens et advenir, et a leurs lieuxtenans,
que de nostre presente grace et octroy, lessent et seuffrent user
paisiblement lesdits habitans et commune. Car ainsi nous plaist il
estre fait et ausdits habitans et commune l’avons octroyé et
octroyons par ces presentes, de grace especial et de nostre certainne
science. Et que ce soit ferme chose a tousjours, nous avons fait
mectre nostre scel a ces lectres. Donné à Rouvre le XXIIe jour
du mois de septembre, l’an de grace mil trois cens quatrevins
et unze. Ainsi signé par Monseigneur le Duc. J. le Mol.
Extrait au vray original
par moy notaire soubscript


