In nomine sancte et individie Trinitatis
notum sit omnibus fidelibus tam presentibus quam futuris quod
Symon de Nielfa donavit deo et monachis de savigneio terram
de valle bric essart in abbatiam construendam in honore
sancte dei genitricis mariae sanctique Johannis baptistae concedente
uxore sua eva ad cujus dotalicium pertinebat locus ille filiisque
ipsius symonis milone videlicet primogenito Gaufrido et
amaurico et Sancelina sorore eorum. Huic donationi cum
multa benevolentia et alacri corde assensum prebentibus.
Addidit etiam ut monachi de nemoribus quaecumque in vici
nia illius loci habebat sufficienter acciperent ad calefaciendum
domos aedificendum et ad coetera opera sua quibus indigent
herbamque et pastionem eorumdem nemorum in alimentum armen
torum et pecorum. Quin etiam totam terram de essarto roberti
dedit eis perpetuo jure ad colendum retenta sibi campiparte.
hoc quoque concessit ut quicumque de hominibus suis alicuid
huic monasterio in elemosina conferre voluerit sive sit
terra sive pratum sive vinea sive quod libet aliud quod sit de
potestate et feodo ilius sine omni contradictione et ca
lumpnia faciat. Testes sunt arnulfus de arsit et Symon
filius ejus Hildiunus pilet hugo et nicholaus filius ejus Renaldus
de clois Theo frater ejus Symon filius ejus Wilhelmus helmere Wilhelmus
de apelgart Gauterius rufus Gaucherius de buisson Gaufridus de
cabrosa. Et ego Gaufridus qui scripsi.
Signum Symonis Signum Hersendis uxoris ejus
Hujus cartule monimentis tam posterorum quam presencium noticie demons
tramus quod Symon de Gomez sancti Spiritus munere cornpunctus de quadam terra sua quam
habebat aput Huaneriam, in feodo Symonis de Nielfa Christum fecit liere
dem donans eam in elemosina totam sicut tenebat, monachis sancte Marie de 
Valle Sarnei uxore sua Hersende non solum assentiente sed ut hoc faceret ad
adnitente et admonente. Hujus donationis testes existunt Rogerius de sancto
Remigio ; Evrardus de Trenbleio ; Teonius de Auteil et Barlholome
us frater ejus ; Rogerius de Buxeria ; Paganus de Pisis


