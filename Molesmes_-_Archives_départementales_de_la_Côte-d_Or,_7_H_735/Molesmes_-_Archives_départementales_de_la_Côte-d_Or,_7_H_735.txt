ont ces présentes lettres, frères droués, abbés et touz li couvenz de l’eiglise de Moloimes, salut en notre seigneur, saichent
A touz ces qui verront t
tuit que cum Lamberz diz Bodins de Ricé et Margueire sa famme aient donné, quicté et outroié en pure aumone à nos et à notre esglise de Mo
loimes lour maison qui fu Garchom, une grange lour devant leu four, lour maison selonc Robert Saussier, lour grange dou pont, lour grant
maison neuve, lour vigne de Vaulongrand, lour vigne qui fu Galam en Chanses, lour vigne quandit rotie, lour grant vigne de Chanses
lour vigne des Crees, lour vigne dou Chatele, lour vigne de la Noie Boissellainge, lour pré de la Cortainne, lour pré dou Brinant, tele
partie cum il ont au pré avec les malades, leu quart de demie seyée avec Sauget, une seyée dou pré Boton, lour pré dessus la Folie, lour lonne
selonc leu molim, la terre derrière la maison neuve, lour meis devant leu molim, lour meis qui est en Auterive et nos aient quicté
touz les harietaiges qui movoient de notre esglise et que nos lour avoiens donnez à lour vies et avec tout ceu il nos aient donné
et quicté, touz lour autres biens meubles et non meubles queconque il soient et en queconque leu il fuent. Des quelx chouses, harietaiges
et meubles il se sunt ja desvectuz et en ont revectu frère Pierre Moinne, notre chamberier en nom de nos et de notre esglise de Moloi
mes. Nos, pour la grant affection et la grant devocion que li diz Lamberz et sa famme ont ades heue et ancor ont à notre esglise, les a
compaignons et façons participans de touz les biens qui seront faiz deci en avant en orasons et en aumones en notre esglise et es membres
de celi et lour prometons à un chascun d’aux autant de servise après lour décès comme à l’un de noz frères, et en récompansacion
dou don devant dit fait à nos, nos donons et outroions au devant dit Lambert et à margueire sa famme tant qu’il vivront chascum
an dix muix de vin bon et souffisant à la mesure de Moloimes et prévande de pain conventual chascun jour pour un chascum
prévande de pain à converse chasun jour pour lour beasse et vint et cinc soz tornois chascun an pour un porc vif et toutes les
semaines quand maingera char, doux fromaige prévandaux, et en la karoime et ans avanz chascun jour quatre arans ou doux generaux
tex cum li couvenz aura, et chascun am un bichet de pois et un bichet de fèves à la mesure de Moloimes. Et chascune semaine
une charreté de buiche et doux soz tornois por pictance à paier à aux leu diemonge, et chascun am doues paires de bones robes soufisantes
à aux tant qu’il vivront ansamble, et l’um daux mort, cil qui sorevivra aura soulemant la moitié de toutes ces chouses devant dites, fors
que tant qu’il aura prévande de pain à  converse avec la soue prévande de pain conventual por sa beasse. C’est à savoir cils deux qui sorevivra aura sa single
prévande de pain conventual et prévande de pain à converse per sa beasse, cinc muix de vin chascun am, en la quinzaine une charreté de buiche, chascune
semaine douze deniers et un fromaige prévandau ou chascun jour tel généraul comme li couvenz aura chascun am un metion de pois et un
de fèves et douze soz et six deniers pour un porc. Et aux morz, nos et notre esglises seront quicte et assos de ce dom devant dit
fait audiz Lambert et Margueire sa famme à lour vies si cum il est dessus dit. Et toutes ces chouses devandites si cum elles sont ci dessus
escrites et devisées mot a mot, nos lour promettons à tenir et a garder fermemant sans corrumpre et sanz aler ancontre en tout am partie
de queque chouse que ce soit. Et pour ceu que ceu soit chouse ferme et estanble, nos, abbés et li couvenz de Meloimes devant dit avons
mis noz seaux en ces lettres que furant faites l’am notre seignour mil doux cenz quatre vinz et six, leu sambadi après les hui
tannes de la tout sainz et les avons bailiés et délivrés au commandemant dou dit Lambert et sa famme saelées de noz seaux


