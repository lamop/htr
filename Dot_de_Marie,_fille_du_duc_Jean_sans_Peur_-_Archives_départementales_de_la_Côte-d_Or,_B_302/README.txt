Dot de Marie, fille du duc Jean sans Peur - Archives départementales de la Côte-d’Or, B 302

Source : https://archives.cotedor.fr/v2/site/AD21/Apprendre/Atelier_du_chancelier_Rolin/Paleographie/Groupe_debutants/Documents_etudies_en_2008-2009/Document_1_-_Dot_de_Marie_fille_du_duc_Jean_sans_Peur

transcription effectué au sein du groupe de l'Atelier du Chancelier Rollin
upload le 17/05/2020
Date : 05/05/1415
Langue : vieux français
