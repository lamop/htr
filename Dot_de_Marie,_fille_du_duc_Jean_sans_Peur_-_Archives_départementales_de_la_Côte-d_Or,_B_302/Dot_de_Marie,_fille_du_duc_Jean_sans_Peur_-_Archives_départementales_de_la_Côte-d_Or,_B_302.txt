C'est l'inventoire et declaracion des joyaux et vaisselle d'or et d'argent, habillemens de chapelle, robes de draps d'or et de soye, et de draps de lainne
fourrees d'ormines, de gris et de menu vair, charioz et cuerres couvers, chambres de draps de haulte lisse, linges, chevaux haquenees, celles et
harnoiz couvers et garniz de draps de soye et de drap de laine et autres choses que monseigneur Jehan, duc de Bourgoingne, conte de Flandres, d'Artois et de Bourgoingne
a baillees aujourduy Ve jour de may mil CCCC. et quinze, a dame Marie sa fille contesse de Cleves et delivrees a monseigneur Adolph conte de
Cleves et de Marke, son seigneur et mary, auquel mondit seigneur a rendu a Dijon ladite dame Marie, sa femme, pour l’emmener en son pais de Cleves. Ledit
inventoire fait les jour et an que dessus en l'ostel de mondit seigneur le duc audit lieu de Dijon par maistre Jehan Bonost le jeune, secretaire, et Jehan
Gueniot clerc des comptes de mondit seigneur le duc, en la presence de maistres Jehan Chousat et Dreue mareschal conseillers et maistres des comptes
de mondit seigneur, a ce expressement ordonnez par mondit seigneur de Bourgoigne, present Hainglequin de Beth chambellan du dit monseigneur de Cleves aussi ordonné
estre present par ycellui monseigneur de Cleves.
Et premierement, joyaux et aornemens de chapelle
Une croix d'argent doree ou il a un crucify et sont les croisons en facon de fleur de liz esmaillez de grains de rouge cler et de blanc, armoyee, aux armes de
mondit seigneur le duc pesant trois mars sept onces dix neuf estellins maille,
Item un calipce d'argent doré esmaillé en la bosse, ensemble la platine esmaillee aussi pesans deux mars quinze estellins,
Item deux chandelliers de chapelle d’argent doré, pesans huit mars trois onces douze estellins ob.
Item une paix d’argent doree et esmaillee pesant sept onces douze estellins ob.
Item un eaubenoistier et un aspargeour tout d'argent doré pesant trois mars cinq estellins,
Item une clochette d'argent doree pesant dix onces dix estellins,
Item deux petites aiguieres d'autel d'argent doré, signees dessus pour la differance de mettre vin et eaue, pesans un marc, 
Item une boiste d’argent couverte a mettre pain a chanter, pesant demi marc,
Item un amit, une aube, une chasuble de drap de soye de Damas, vermeil signee, garny d’un orfroy de brodme d'or, a ymages des apostres,
ensemble l’estole et manuple,
Item un drap de Damas vermeil ou il a un cruxify et deux ymages de Nostre Dame et de saint Jehan bien richement brodé, pour parement dessus l'autel
et un autre drap de mesmes pour parement devant l'autel au dessoubz, doublez de toille perse
Item un corporal en un estuy de drap de Damas vermeil,
Item un marbre blanc enchassé en bois
Item un messel entier en petit volume de tout le temps garni de deux fermeillez d’argent dorez et esmaillez aux ymages de sainte Katherine et
sainte Marguerite, couvert de drap de Damas vermeil, et doublé de satin.
Item une bande de drap de Damas vermeil, doublé de toille perse pour mettre davanz l'autel, garnie de franges,
Item deux heles autrement dites custodes pour mettre a l'environ de l'autel, de tafetaz vermeil roye, garnies de franges vermeilles et blanches
de boucles et de cordes pour tendre et destendre,
Item deux courtines de tafetas pour tendre l'oratoire
Item un petit coffre couvert de cuir ou sont lesdiz ornemens


