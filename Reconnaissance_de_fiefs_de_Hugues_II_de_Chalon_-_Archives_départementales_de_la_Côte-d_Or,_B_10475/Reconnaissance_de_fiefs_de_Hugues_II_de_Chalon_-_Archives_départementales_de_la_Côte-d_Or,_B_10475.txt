Coppie
En nom de notre seigneur, amen, l'an de l'incarnacion
d'icelluy mil CCCC et deux, le darrain jour de may
nous, Droin Mareschal et Jehan Bonost, coadiucteurs
de Andry Estienne de Franay, tabellion de Dijon pour
monseigneur le duc de Bourgoingne, faisons savoir
à tous présens et avenir qui cest présent transcript
verront, nous avons veu, tenu et leu de mot à mot
unes lettres saines et entières de seel et d'escripture
et non aucunement vicieuses, seellées en cire
vermeille et en quehue double du seel
de feu noble et puissant seigneur monseigneur Hugues
de Chalon, jaidis sire d'Arlay, contenant la forme
qui s'ensuit. Je, Hugue de Chalon, sire d'Arlay
faiz savoir à tous que je confesse par ces présentes
lettres moy estre homme liege et féal de très hault
noble et très puissant seigneur monseigneur Phelippe
mon très cher seigneur, filz du roy de France, duc de
Bourgoingne, conte de Flandres, d'Artois, de Bourgoingne
palatin, seigneur de Salins, conte de Rethel et
seigneur de Malines, à cause de son conté de Bourg
et de luy tieng et confesse tenir en fief et en


houmaige ligement, ligement les forteresses
chasteaulx, dongeons, bourgs, villes, noblesses, fiez
rerefiez et les choses qui s'ensuigvent. Et
premièrement, je tieng de mondit seigneur à
cause que dessus les chasteau, dongeon, bourg, ville
et forteresse d'Arlay assiz en la diocèse de Besançon
ensemble la chastellenie, villes, villages, propriétez
terres, appartenances et appendisses, environ, fiefz
rerefiefz, dominacions et seigneuries, usances, justices
toutes ressors, batiz et franchises et tout ce entièrement
que j'ay, tien et puis avoir et tenir es lieux, finaiges
et territoires des villes cy après devisées, c'est assavoir
Arlay, la ville de Courcelles, Chasoy, Joyant, Platenay
Saint-Germain, Champ Dommange, Lombart, Vincent
Machefain, Froideville, Bestent, Requano le Grant
et Requano le petit, en hommes, en meix, en
tenemens, en justice toutes jurisdictions haulte
moyenne et basse, meis et mixte impere, noblesses
dominacions et seigneuries quelxconques, en bois, en
forestz, en estans, en moulins, en terraiges, rentes
censes et en toutes et singulières autres choses et
la garde que j'ay ou prieuré de Saint-Germain
et celle que j'ay ou prioré du Sauvement à


cause de ce que moy y puet appartenir et généralment
tout ce que je tien ; puis et doys avoir et tenir en mon
domaine à cause desditz chasteau, dongeon, bourg
et ville d'Arlay en ladite chastellenie, appartenances
et appendisses d'icelle, en toutes et singulières choses
quelconques et par quelque nom que elles
soient nommées, spécifiées ou appellées. Item
s'ensuivent cy après les fiez que l'on tient de moy
à cause dudit Arlay et lesquelx je tien de
mondit seigneur à cause de sondit conté de
Bourgoingne. Et premièrement le fiez que tient
de moy messire Jaques de Vergey, syre d'Autrey
à cause de sa femme, c'est assavoir les chasteau
et forteresse de La Murée, la ville dudit lieu, ensemble
la chastellie, terres, usaiges, villaiges, appartenances
et appendisses d'icellui, fiez, rerefiez, noblesses, seigneurie
usances, ressors, batiz souverainnetez que ledit
messire Jaques tient et doit tenir de moy à cause
que dessus tant en fié que rerefié comme en
autres choses quelxconques, ensemble les justices
à lui esdiz lieux appartenans et ressortissans
audit arlay. Item le fié des chasteau, bourg
et dongeon de l'Estoille que tient de moy


messire Vauthier de Freloiz, sire de Saint-Germain à
cause de sa femme, ensemble tout ce que esdits
chasteau et bourg, chastellenie, terres, villaiges
appartenances d'icelles, justices, toutes usances
dominacions et seigneuries, ressors, batis et
souveraineté que ledit messire Vauthier tient
et doit tenir de moy à cause dudit Estoille, en
fié, rerefiez, tant à Plannoisal, Quintigny
Valières, Mabourgal comme ailleurs, estant des
appartenances dudit Estoille en quelque manière
que ce soit. Item le fié du chastel de
Largillaiz que tient de moy messire Philibert
seigneur de Boffremmont, ensemble la chastellenie
toutes les appartenances, villes, bois, estangs
noblesse, jurisdictions, usances, ressort, batiz
et souveraineté, fiez, rerefiefz et autres choses quelxconques
que à cause dudit chasteau me appartient, pevent
et doivent appartenir, ensemble les justices esdits
lieux appartenans et ressortissans audit Arlay
Item le fié du chasteau de Montjay et la
bassecourt dudit chasteau, ensemble la chastellenie
les villes, boiz, estangs, fiez, rerefiez, noblesses


seigneuries, usances, ressort, batiz et souveraineté
et autres choses quelxconques appartenans audit
chasteau que tient de moy Estienne de Monsalgeon
ensemble la justice esdiz lieux appartenans
et ressortissans audit arlay. Item le fié de ce
que messire Hugues de Veudrey tient de moy en
ladite chastellenie de Montjay et généralement
tous autres fiez et rerefiez que l'on tient et doit
tenir de moy à cause dudit Montjay. Item
le fiez du chastel de Jouceal que tient de
moy Jaquemart de Villette à cause de Jehanne
sa femme, ensemble les villes, hommes et
femmes et autres choses audit lieu de Joucel et appartenances
qu'il tient puet et doit tenir de moy tant en
fié que rerefié, noblesses, seigneuries, usances
ressort, batiz et souveraineté et autres choses
quelxconques ensemble la justice audit lieu, appartenans
et ressortissans audit Arlay. Item le fiez
du chastel et forteresse de Coge que tiennent
de moy chascun pour sa partie Jehan de Coge, alias
Galafin, et les enffans de feu messire Jehan Galaffin
jaidiz sire dudit lieu en partie, ensemble tout


ce que en ladicte ville dudit lieu, terres, appartenances
et es appendisses d'icelles ilz tiennent et doivent
tenir de moy tant en fié comme rerefiez
en noblesses, seigneuries et dominacions, usances
ressort, batiz et souveraineté et autrement, en
quelque nature que ce soit, ensemble la justice
eulx audit lieu appartienans et ressorcissant
audit Arlay. Item le lié de ce que messire
Jaques de Vienne syre de Longvy tient de
moy à Beaulvoy et ce qu'il a à Chilley et
es appartenances desdiz lieux en fiez, rerefiez
seigneuries, juridictions, noblesses, ressors, batiz
et souveraineté et autres choses en quelque manière
que ce soit, ensemble les justices qu'il puet
avoir esditz lieux, appartenans et ressortissans audit
Arlay. Item le fié de tout ce que tient de
moy messire Guillaume de Darbonnay à
a Arlay et en tout la chastellenie appartenances
et appendisses d'icelle en quelque manière que ce
soit. Item le fié de ce que messire Hugues
de Vuillafans tient de moy audit Arlay


