Corpus de transcriptions hétérogènes pour des expériences de reconnaissance de l'écriture manuscrite

Les manuscrits et les transcriptions sont issues de différents établissements
Les Archives des Côtes d'Or : https://archives.cotedor.fr/v2/site/ad21/ dans le cadre des ateliers de paléographie du Chancelier Rolin des Archives départements des Côtes d’Or
Les Archives départementales de l’Ain : http://www.archives.ain.fr/ avec le travail de Florence Beaume, directrice des Archives Départementales.
Les Archives départementales de la Haute-Saône : http://archives.haute-saone.fr/ par les élèves du cours de paléographie des Archives départementales de la Haute-Saône de 2013-2014.
Les Archives départementales des Yvelines : https://educarchives.yvelines.fr/ avec le travail de Françoise Jenn. 
Les Archives départementales de l'Indre : https://www.indre.fr/archives avec le travail de M. du Pouget, Archiviste, paléographe, Directeur des Archives départementales de l'Indre.
Les Archives Nationales http://www.archivesnationales.culture.gouv.fr/
Wikimedia Commons https://commons.wikimedia.org/ Les textes issus de Wikimedia Commons sont disponibles sous licence Creative Commons Attribution-partage dans les mêmes conditions

Projet coordonnée par Pierre Brhcard. Merci à Hugo Regazzi pour son aide lors
de la réalisation de ses premières données.

Contact : archive@lamop.fr

Version : 
11-2020 : 0.1 : Mise au propre, choix des formats de fichiers, sélection des transcriptions
