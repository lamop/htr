Cette image est issue de cette URL : https://commons.wikimedia.org/wiki/File:Acte_qui_d%C3%A9clare_le_don_par_Charles_VI_%C3%A0_son_fr%C3%A8re_Louis_1_-_Archives_Nationales_-_K532-B_n%C2%B017.JPG
Extrait
This file was provided to Wikimedia Commons by the Archives Nationales as part of a cooperation project with Wikimédia France.
This work is in the public domain in its country of origin and other countries and areas where the copyright term is the author's life plus 100 years or fewer. 

La Transcription est sous licence Creative Commons By-SA https://creativecommons.org/licenses/by-sa/3.0/deed.fr
Source : https://fr.wikisource.org/wiki/Page:Acte_qui_d%C3%A9clare_le_don_par_Charles_VI_%C3%A0_son_fr%C3%A8re_Louis_1_-_Archives_Nationales_-_K532-B_n%C2%B017.JPG
