Cette image est issue de cette URL : https://commons.wikimedia.org/wiki/File:Le_tr%C3%A9sorier_et_les_chanoines_de_la_Sainte-Chapelle_de_Paris_promettent_de_c%C3%A9l%C3%A9brer_une_messe_annuelle_du_Saint-Esprit_pour_le_duc_Jean_de_Berry_chaque_10_septembre_de_son_vivant,_et_apr%C3%A8s_sa_mort..._-_Archives_Nationales_-_J-187-15.jpg?uselang=fr
Extrait
This file was provided to Wikimedia Commons by the Archives Nationales as part of a cooperation project with Wikimédia France.
This work is in the public domain in its country of origin and other countries and areas where the copyright term is the author's life plus 100 years or fewer. 

La Transcription est sous licence Creative Commons By-SA https://creativecommons.org/licenses/by-sa/3.0/deed.fr
Source : https://fr.wikisource.org/wiki/Page:Le_tr%C3%A9sorier_et_les_chanoines_de_la_Sainte-Chapelle_de_Paris_promettent_de_c%C3%A9l%C3%A9brer_une_messe_annuelle_du_Saint-Esprit_pour_le_duc_Jean_de_Berry_chaque_10_septembre_de_son_vivant,_et_apr%C3%A8s_sa_mort..._-_Archives_Nationales_-_J-187-15.jpg
