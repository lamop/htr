Je, Geoffroy de Beaulvoir, seigneur dudit lieu et de Mussy la fosse, certifie a tous que comme Anthoine de Saigny seigneur de Saffres eust fait mettre en
sa main les chasteaul terres rentes proffiz revenus et emolumens dudit Mussy et appartenences d’icelluy pour deffault de devoir de fied et hommaige
a lui non fait dudit Mussy et de ses dites appartenances mouvans de son fied a cause de son chasteaul et seignourie dudit Saffres comme il dit et
maintient, lequel fied et hommaige je differay faire jusques il m’eust apparu a suffisance que icelluy Mussy feust du fied d’icelluy Anthoine
seigneur de Saffres. Ainsi est que asses tost apres ladite mainmise, icelluy Anthoine de Saigny me monstra pluseurs lettres et anciens
enseignemens par lesquelx il me apparut suffisamment lesdiz chasteaul et appartenances dudit Mussy estre et mouvans du fied dudit Anthoine
de Saigny a cause de son dit chasteaul de Saffres et les en cognoiz et confesse tenir. Et pour ce, aujourd’uy je ledit Geoffroy suis entré en
foy et hommaige dudit Anthoine desdiz chasteaul et seignourie dudit mussy la fosse et des appartenances et appendences d’icelluy et promis en faire
les services en tel cas appartenans et luy en bailler mon denombrement et declaration dedans temps deu. Tesmoing mon seing manuel et
seel de mes armes cy mis le cinquieme jour du mois de juing, l’an mil quatre cens cinquante et quatre.


