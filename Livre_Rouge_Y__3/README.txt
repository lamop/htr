Châtelet de Paris. Y//3 -  Livre Rouge

https://www.siv.archives-nationales.culture.gouv.fr/siv/UD/FRAN_IR_056373/c-2xp1c58jd-1w19uv1g8sus6

transcription par Hugo Regazzi Page 1-60
upload le 29/11/2019

Liste des abbréviations

App(b)eillees = appareillées
Bouch'r = boucher
Com'e = comme
Laut' = l'autre
Daut' = d'autre
Par' = Paris
Com'andem's = commandemens
Cestass' = c'est assavoir
Envir' = environ
App(b)tient = appartient
Ass' = assavoir
Ch'un = chascun
Oucas = ou cas
Enioint = enjoint
T'spas = trespas
Entiere = entière
Aleurs = à leurs

Les apostrophes représentent les tildes et "(b)" signifie que le jambage de la lettre précédente est barré.

